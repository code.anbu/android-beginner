package ei.eseptiyadi.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<String> countries;
    private ArrayList<String> deskripsi;
    private ArrayList<Integer> gambar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialViews();
    }

    private void initialViews() {
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.rv_listnegara);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        countries = new ArrayList<>();
        countries.add("Indonesia");
        countries.add("Palestina");
        countries.add("Saudi Arabia");
        countries.add("Singapore");
        countries.add("Malaysia");

        deskripsi = new ArrayList<>();
        deskripsi.add("https://id.wikipedia.org/wiki/Indonesia");
        deskripsi.add("https://id.wikipedia.org/wiki/Palestina");
        deskripsi.add("https://id.wikipedia.org/wiki/Arab_Saudi");
        deskripsi.add("https://id.wikipedia.org/wiki/Singapura");
        deskripsi.add("https://id.wikipedia.org/wiki/Malaysia");

        gambar = new ArrayList<>();
        gambar.add(R.drawable.indoensia);
        gambar.add(R.drawable.palestina);
        gambar.add(R.drawable.arabsaudi);
        gambar.add(R.drawable.singapur);
        gambar.add(R.drawable.malaysia);

        RecyclerView.Adapter adapter = new AdapterListNegara(countries, deskripsi, gambar);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {

            GestureDetector gestureDetector = new GestureDetector(getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {

                @Override public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

            });

            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                View child = rv.findChildViewUnder(e.getX(), e.getY());
                if(child != null && gestureDetector.onTouchEvent(e)) {
                    int position = rv.getChildAdapterPosition(child);
                    Toast.makeText(getApplicationContext(), "Negara yang dipilih " + countries.get(position) + " Deskripsi " + deskripsi.get(position), Toast.LENGTH_LONG).show();
                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });


    }
}