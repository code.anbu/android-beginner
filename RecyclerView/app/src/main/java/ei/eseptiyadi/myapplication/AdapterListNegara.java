package ei.eseptiyadi.myapplication;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class AdapterListNegara extends RecyclerView.Adapter<AdapterListNegara.ViewHolder> {


    private ArrayList<String> countries;
    private ArrayList<String> deskripsi;
    private ArrayList<Integer> gambar;

    public AdapterListNegara(ArrayList<String> countries, ArrayList<String> deskripsi, ArrayList<Integer> gambar) {
        this.countries = countries;
        this.deskripsi = deskripsi;
        this.gambar = gambar;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv_country.setText(countries.get(position));
        holder.tv_descountry.setText(deskripsi.get(position));
        holder.pic_negara.setImageResource(gambar.get(position));
    }

    @Override
    public int getItemCount() {
        return countries.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_country;
        private TextView tv_descountry;
        private ImageView pic_negara;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_descountry = (TextView)itemView.findViewById(R.id.tv_countrydesc);
            tv_country = (TextView)itemView.findViewById(R.id.tv_country);
            pic_negara = (ImageView)itemView.findViewById(R.id.picCountry);

        }
    }
}
